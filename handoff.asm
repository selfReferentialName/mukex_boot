	;; handoff.asm
	;; hand over controll to MUKEX

section .bss
extern prehandoffcr3
extern handoffcr3
extern handoffstart
extern prehandoffpt0
extern handoffpt0

section .text align=4096
global handoff

handoff:mov rax, handoff
	mov rdi, [handoffcr3]
	mov rsi, rax
	cli
	mov rsp, [handoffstart]
	mov rbp, [handoffpt0]
	mov r8, [prehandoffpt0]
	shr rax, 12
	mov rbx, 0xffffff80
	shl rbx, 32
	add rax, rbx
	inc byte [rax]
	mov rax, [prehandoffcr3]
	mov cr3, rax
	mov rcx, 0x80
	shl rcx, 32
	mov dl, 1
loop0:	cmp [0xffffff80000000000 + rcx - 1], dl
	je label0
	loop loop0
	mov rax, rsp
	shr rax, 12
	mov rbx, [rdi + rax * 8]
	mov [r8 + rax * 8], rbx
	mov r9, rsp
	mov rsp, label1
	and rsp, 0x0fff
	shl rax, 12
	add rsp, rax
	jmp rsp
label1:	mov rcx, label0 - hoffist
	mov rsi, hoffist
	mov rdi, r9
	sub rdi, rcx
	rep stosb
	sub rdi, label0 - hoffist
	jmp rdi
	; done :)

hoffist:mov cr3, rax

label0:	mov rax, rcx
	shl rax, 12
	add rax, 3
	mov [prehandoffpt0 + 8], rax
	invlpg [0x1000]
	mov [0x1000], rbx
	sub rax, 3
	mov rbx, rax
	loop loop0
