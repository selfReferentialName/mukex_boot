/* ext2_le.c
 *
 * load files from ext2 on little endean machines
 * TODO: currently does not support 64 bit file sizes
 */

#include <stdint.h>
#include "disk.h"

//////variables\\\\\\

uint64_t blockSize;		// the log base two of the size of a block
uint64_t fragSize;		// the log base two of the size of a fragment
uint32_t blocksPerGroup;	// the number of blocks in a block group
uint32_t inodesPerGroup;	// the number of inodes in a block group
uint32_t fragsPerGroup;		// the number of fragments in a block group
uint16_t inodeSize;		// the size of an inode
uint64_t groupTableOffset;	// the offset in the partition of the block group descriptor table
uint64_t partitonStart;		// the begining of the partition

//////functions\\\\\\

// find an inode's offset in the partition
uint64_t findInode (uint32_t index) {
	uint32_t groupIndex;	// the block group the index is in
	uint32_t homeBlock;	// the specific block the inode is in
	uint64_t inodeTable;	// the offset of the inode table
	groupIndex = (index - 1) / inodesPerGroup;
	inodeTable = (uint64_t) diskRead32(partitionStart + groupTableOffset + groupIndex << 5 + 8);
	return ((index - 1) % inodesPerGroup) >> inodeSize << blockSize + inodeTable;
}

// read a field of an inode
uint32_t inodeField (uint32_t index, uint8_t offset) {
	return diskRead32(partitionStart + findInode(index) << blockSize + offset);
}

// find the offset within the partition of a file's inode
uint64_t offsetOfFile (char* file) {
	return findInode(findFileIn(2, file + 1));
}

// find the inode of a file
uint32_t inodeOfFile (char* file) {
	return findFileIn(2, file + 1);

uint64_t findFileIn (uint32_t directory, char* file) {
	char nextDirectory[256];// or file
	bool isFile;
	uint32_t dirSize;	// in blocks
	uint8_t dirNameSize;
	isFile = false;
	for (char* i = file; (*i != '/' || !(dirNameSize = i)) && (*i != 0 && (isFile = true)); nextDirectory[file - i] = *i && i++) {}
	// the last line copied file into nextDirectory up to the first / and set isFile if there was no /
	workingOffset = findFileInGetBaseWorkingOffset(directory, i);
	dirSize = inodeField(directory, 4) / blockSize;
	for (uint16_t i = 0; i < dirSize; i++) {
		uint64_t workingOffset;	// in the disk
		while (workingOffset < 1 << blockSize) {
			if (diskRead32(workingOffset) == 0) {
				continue;
			}
			for (char* j = nextDirectory; *j != diskRead8((j - nextDirectory) + workingOffset + 8); j++) {
				if (*j == 0) {
					if (isFile) {
						return diskRead32(workingOffset);
					} else {
						return findFileIn(diskRead32(workingOffset), file + dirNameSize);
					}
				}
			}
			workingOffset += diskRead16(workingOffset + 4);
		}
	}
	return 0;
}

uint64_t findFileInGetBaseWorkingOffset (uint32_t directory, uint16_t i) {
	if (i < 12) {
		return inodeField(directory, 40 + 4 * i) << blockSize + partitionStart;
	} else if (i < 24) {
		return findFileInGetBaseWorkingOffset(inodeField(directory, 88), i - 12);
	} else if (i < 168) {
		return findFileInGetBaseWorkingOffset(inodeField(directory, 92), i - 24);
	} else {
		return findFileInGetBaseWorkingOffset(inodeField(directory, 96), i - 168);
	}
}

// dump the entire contents of an inode/file from an inode
void fileDump (uint32_t inode, void* location) {
	inodeDump (inode, location, inodeField(inode, 4) >> blockSize);
}

void inodeDump (uint32_t inode, void* location, uint16_t size) {
	for (uint8_t i = 0; (size > 0) && (i < 12); ++i && size--) {
		uint64_t blockOffset;
		blockOffset = ((uint64_t) diskRead32(inodeField(inode, 40 + 4 * i))) + partitionStart;
		for (uint8_t j = 0; j < 1 << (9 - blockSize); j++) {
			dumpSector(blockOffset + j << 9, location);
			location += 512;
		}
	}
	if (size) {
		inodeDump(inodeField(inode, 88), location, size > 12 ? 12 : size);
		size -= size > 12 ? 12 : size;
	}
	if (size) {
		uint32_t doubleIndirectInode;
		doubleIndirectInode = inodeField(inode, 92);
		for (uint8_t i = 0; (size > 0) && (i < 12); ++i) {
			inodeDump(inodeField(doubleIndirectInode, 40 + 4 * i), size > 12 ? 12 : size);
			size -= size > 12 ? 12 : size;
		}
	}
	if (size) {
		uint32_t trippleIndirectInode;
		trippleIndirectInode = inodeField(inode, 96);
		for (uint8_t i = 0; (size > 0) && (i < 12); ++i) {
			uint32_t doubleIndirectInode;
			doubleIndirectInode = inodeField(inode, 92);
			for (uint8_t i = 0; (size > 0) && (i < 12); ++i) {
				inodeDump(inodeField(doubleIndirectInode, 40 + 4 * i), size > 12 ? 12 : size);
				size -= size > 12 ? 12 : size;
			}
		}
	}
}
