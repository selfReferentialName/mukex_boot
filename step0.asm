	;; step0.asm
	;; get the bootloader into a sane state and process the multiboot information

[BITS 32]

section .bss align=4096
global cmdline
global mmpdpt
stack:	resb 32664
cmdline:resb 64
bootldr:resb 32
memmap:	resd 1
mmpt0:	resb 4096
mmpd0:	resb 4096
mmpdpt:	resb 4096
mmnopg:	resb 4096
ebcr3:	resb 4096 * (1 + 1 + 4 * (1 + 512))
uspg:	resd 8
blah:	resd 1

section .rodata align=4096
mmnopt:	times 512 dq mmnopg + 3
mmnopd:	times 512 dq mmnopt + 3

section .multiboot align=4096			; multiboot2 header
MB2HEAD_MAGIC EQU 0xe85250d6
MB2HEAD_ARCH EQU 0				; x86 32 bit or x86_64 EFI
MB2HEAD_SIZE EQU mb2head.end - mb2head.start
MB2HEAD_CHECK EQU 0x100000000 - MB2HEAD_MAGIC - MB2HEAD_ARCH - MB2HEAD_SIZE

mb2head:
.start:	dd MB2HEAD_MAGIC
	dd MB2HEAD_ARCH
	dd MB2HEAD_SIZE
	dd MB2HEAD_CHECK

	dw 1					; multiboot information request
	dw 0
	dd 20
	dd 1					; get a boot command line
	dd 2					; get a boot loader name
	dd 6					; get a memory map

	dd 0xdacaabba				; padding

	dw 4					; console flags
	dw 0
	dd 12
	dd 0x3					; ega text support, require a console

	dd 0xdeadbeef				; padding

	dw 5					; framebuffer settings
	dw 1
	dd 12
	dd 80					; width
	dd 25					; highth
	dd 0					; depth -- 0 means character mode

	dd 0xfadebaaa				; padding

	dw 3					; entry address
	dw 0
	dd 12
	dd _start

	dd 0xbada5500				; padding

	dw 0					; last tag
	dw 0
	dd 8
.end

section .rodata

gdt:						; gdt for 64 bit, not much useful here
.null:	equ $ - gdt				; for the CPU, useless
	dw 0
	dw 0
	db 0
	db 0
	db 0
	db 0
	db 0
.code:	equ $ - gdt				; for instruction accesses, segment 0x08
	dw 0
	dw 0
	db 0
	db 0x9a
	db 0x20
	db 0
.data:	equ $ - gdt				; for data access, segment 0x10
	dw 0
	dw 0
	db 0
	db 0x92
	db 0x00
	db 0

gdtptr:	dw $ - gdt - 1				; pointer to the gdt for lgdt
	dq gdt

section .text
global _start
extern step1

_start:	; TODO: when more boot formats are supported, check to find out which one it is
	; TODO: send debugging info to the serial port
	mov ax, 0x0440
	mov edi, 0xa0000
	mov ecx, 0x20000
	rep stosw				; show that we have indeed been bootloaded
	mov esp, cmdline			; setup stack
	mov eax, mmpd0
	mov [mmpdpt], eax
	mov eax, mmpt0
	mov [mmpd0], eax
	mov eax, 0x80000000
	mov [mmpdpt + 4], eax
	mov [mmpd0 + 4], eax			; set up initial paging for the memory map
	mov al, 0x77
	mov ecx, 1024
	mov edi, mmnopg
	rep stosd				; set up a "null" memory map page
	; parse multiboot info
	add eax, 8
loop0:	mov eax, [ebx]
	cmp eax, 1
	je cpycmd
	cmp eax, 6
	je parsmem
	cmp eax, 2
	je cpybldr
lp0lb0:	add ebx, [ebx + 4]
	cmp dword [ebx], 0
	jne loop0
	cmp dword [ebx + 4], 8
	jne loop0
	; multiboot info parsed
	; pageing is set up, now we just need to enter long mode
	mov eax, cr0
	and eax, 0x1ff3ffd0
	or eax, 0x00000001
	mov cr0, eax
	mov eax, cr4
	and eax, 0x000c0000
	or eax, 0x000106a0
	mov cr4, eax
	mov ecx, 0xc0000080			; EFER
	rdmsr
	and eax, 0xffff00fe
	or eax, 0x00008900
	wrmsr
	mov eax, cr0
	or eax, 0x80000000
	lgdt [gdtptr]
	; make the final jump into long mode
	jmp codeseg:step1

cpycmd:	mov ecx, [ebx + 4]
	sub ecx, 8
	mov esi, ebx
	add esi, 8
	mov edi, cmdline
	rep movsb
	ret

cpybldr:mov ecx, [ebx + 4]
	sub ecx, 8
	mov esi, ebx
	add esi, 8
	mov edi, bootldr
	rep movsb
	ret

probepg:push ebx				; probe the page at eax to make sure that it exists, if not, set eax to 0x1
	mov ebx, [eax]
	not ebx
	mov [eax], ebx
	flush
	cmp ebx, [eax]
	jne noprobe
	ret
noprobe:mov eax, 0x1
	ret

parsmem:mov ecx, [ebx + 4]			; set ecx to the size
	mov edx, [ebx + 8]			; set edx to the size of an entry
	mov esi, ebx
	add esi, 16
	sub ecx, 16
	xor ebp, ebp				; ebp holds the number of defered entries
	; all the above was was simple setup
	; the goal is to create the memory map and fill it out at the same time
	; in order to do this, we need to defer entries
	; there are three loops needed:
	; * one loop goes until it finds a page that is too high for the memory map
	; * one loop that defers pages onto the stack until it finds one for the memory map
	; * one loop that marks the defered pages
	; the loops are run in a meta-loop that goes in the order above
	; as pages that can be used for the memory map are found (i.e. in the first 4 KB), they are noted as potential memory map pages
	; in order to find out where in the memory map we need to go, we use what is in escense a page table crawler
	push ebx
	call walkpg
	pop ebx
	; now we set up paging for the memory map
	mov ecx, 512
	xor ebp, ebp
pmemlp0:cmp [mmpdpt + 8 * ecx - 8], ebp
	je pmlp0l0
	push ecx
	mov eax, [mmpdpt + 8 * ecx - 8]
	sub eax, 8
	mov ecx, 512
pmlp0l1:cmp [eax + 8 * ecx], ebp
	je pmlp0l2
	push ecx
	push eax
	mov eax, [eax + 8 * ecx]
	sub eax, 8
	mov ecx, 512
pmlp0l3:cmp [eax + 8 * ecx], ebp
	jne pmlp0l4
	mov edi, mmnopg
	mov [eax + 8 * ecx], edi
pmlp0l4:mov edi, [eax + 8 * ecx]
	add edi, 3
	mov [eax + 8 * ecx], edi
	loop pmlp0l3
	pop eax
	pop ecx
	mov edi, [eax + 8 * ecx]
	add edi, 3
	mov edi, [eax + 8 * ecx]
pmlp0l6:loop pmlp0l1
	pop ecx
	add eax, 8 + 3
pmlp0l5:loop pmemlp0
	; now we need to set up identity mapped pageing for the lowest 4 GiB
	; we don't map the higher pages yet because we can't be certain that there will be enough space
	mov edi, ebcr3 + 0x1000 + 3
	mov [ebcr3 + 8 * ecx - 8], edi
	add edi, 0x1000
	mov [ebcr3 + 0x1000 + 8 * ecx - 8], edi
	add edi, 0x1000
	mov ecx, 4
pmemlp1:mov [ebcr3 + 0x2000 + 8 * ecx - 8], edi
	add edi, 0x1000
	loop pmemlp1
	mov ecx, 4 * 512
pmemlp2:mov [ebcr3 + 0x6000 + 8 * ecx - 8], edi
	add edi, 0x1000
	loop pmemlp2
	; we also map in the memory map starting at 0xffffffff00000000
	mov eax, mmpdpt + 3
	mov [ebcr3 + 0x800], eax
	; and set cr3
	mov eax, ebcr3
	mov cr3, eax
	; now we are finally done
	ret
	; these are some labels to cover the code for a empty memmap entry from above
pmlp0l0:mov eax, mmnopd + 3
	mov [mmpdpt + 8 * ecx - 8], eax
	jmp pmlp0l5
pmlp0l2:mov ebp, mmnopt + 3
	mov [eax + 8 * ecx], ebp
	xor ebp, ebp
	jmp pmlp0l6

walkpg:	mov ebx, [esi + 16]			; walk to generate the memory map
	cmp ebx, 1
	jne wkpgcnt				; only add real pages
	push eax
	push dword [esi]
	push dword [esi + 4]
	push dword [esi + 8]
	push dword [esi + 12]
	call addpg				; add a memory map tag segment thing
wkpgcnt:add esi, edx
	sub ecx, edx
	jnz walkpg				; keep walking until there are no segments left
	ret


crawlpg:mov eax, [esp + 12]			; get the address in the memory map to eax
	push ecx
	shr eax, 12
	mov ebx, [esp + 8]
	shl ebx, 20
	or eax, ebx
	and eax, 0x000fffff
	mov ebx, eax
	shr ebx, 18
	mov ebx, [mmpdpt + ebx * 8]
	test ebx, ebx
	jnz cpgcnt0				; skip to cpgcnt0 if the page exists
	push eax
	push edi
	push ebx
	call getuspg
	mov edi, eax
	mov eax, 0
	mov ecx, 1024
	rep stosd
	pop ebx
	mov [mmpdpt + ebx * 8], edi
	pop edi
	pop eax
cpgcnt0:and eax, 0x0003ffff
	mov ecx, eax
	shr ecx, 9
	shl ecx, 3
	add ebx, ecx
	mov ebx, [ebx]
	test ebx, ebx
	jnz cpgcnt1				; skip to cpgcnt1 if the page exists
	push eax
	push edi
	push ebx
	push ecx
	call getuspg
	mov edi, eax
	mov eax, 0
	mov ecx, 1024
	rep stosd
	pop ecx
	pop ebx
	mov [ebx], edi
	pop edi
	pop eax
cpgcnt1:and eax, 0x000001ff
	mov ecx, eax
	shl ecx, 3
	add ebx, ecx
	mov ebx, [ebx]
	pop ecx
	mov eax, [esp + 12]
	jnz cpgcnt2				; skip to cpgcnt2 if the page exists
	push eax
	push edi
	push ebx
	push ecx
	call getuspg
	mov edi, eax
	mov al, 1
	mov ecx, 4096
	rep stosb
	pop ecx
	pop ebx
	mov [ebx], edi
	pop edi
	pop eax
cpgcnt2:and eax, 0x00000fff
	or eax , ebx
	ret

getuspg:mov ecx, 8				; get a page we can use for paging (a usable page)
	mov ebx, uspg
gtuplp0:mov eax, [ebx]
	test eax, eax
	jnz gtuplb0				; we found a usable page! time to note that it is used and stuff
	add ebx, 4
	loop gtuplp0
	jmp fnduspg				; we didn't find a usable page, so we need to look into the memory map for it
gtuplb0:xor ecx, ecx
	mov [ebx], ecx
	shr eax, 12
	push eax
	push ecx
	call crawlpg
	inc byte [eax]
	pop ecx
	pop eax
	shl eax, 12
	ret

fnduspg:mov ebx, [esi + 16]			; look in the memory map for a usable page
	cmp ebx, 1
	jne fdpgcnt				; go on to the next page, this one isn't usable
	mov ecx, [esi + 4]
	test ecx, ecx
	jz fdpglb0				; jump if the page is at a low enough address and is therefore usable
	push esi				; if not, defer the page
	inc ebp
	jmp fdpgcnt				; go on to the next page
fdpglb0:mov ecx, [esi + 12]
	test ecx, ecx
	jnz fdpglb1				; page is big (bigger than 4 GiB) and therefore usable
	mov ecx, [esi + 8]
	mov eax, [esi]
	and eax, 0x0fff
	sub ecx, eax
	test ecx, 0xfffff000			; if the page isn't big, check to make sure it actually fits a page
	jz fdpgcnt				; if not, go to the next page without defering
fdpglb1:push dword [esi]			; use the page at [esi] (not esi)
	mov eax, [esi]
	and ax, 0xf000
	add eax, 0x1000				; take it away from what will be passed to addpg
	jnc fdpglb2				; the above add should be a 64 bit add, jump if we don't need to increment the upper 32 bits
	inc dword [esi + 4]
fdpglb2:mov [uspg], eax
	push dword [esi]
	push dword [esi + 4]
	push dword [esi + 8]
	push dword [esi + 12]
	call addpg				; add all the pages but the one that we will return
;	jmp referpg

referpg:pop eax					; take pages off of the defer list
	mov ebx, [eax + 16]
	cmp ebx, 1
	jne rfpgcnt				; this isn't the page we're looking for, move along
	push eax
	push dword [eax]
	push dword [eax + 4]
	push dword [eax + 8]
	push dword [eax + 12]
	call addpg
rfpgcnt:dec ebp					; we took a page off the defer list
	jnz referpg				; if we have more, keep taking them off
	pop eax					; we saved this earlier in fnduspg at fdpglb1
	and ax, 0xf000
	ret

fdpgcnt:add esi, edx
	jmp fnduspg

addpg:	pop eax					; eax holds the return address
	mov [esp + 12], eax
	pop eax
	xor ecx, [esp + 4]
	xor [esp + 4], ecx
	xor ecx, [esp + 4]
	xchg ebx, ecx
	xor edi, [esp]
	xor [esp], edi
	xor edi, [esp]
	mov ecx, [esp + 12]
	test bx, 0x0fff
	jz apglb0				; no need to align to page, it's already aligned
	and bx, 0xf000
	add ebx, 0x00001000
apglb0:	and cx, 0xf000
	test ecx, ecx
	jz apgfc0				; if the size's low 32 bits are zero without the low 12 bits, test for a nonexistant page
	test ebx, ebx
	jnz apglp1				; if the address is above 4 GiB, no need to mark the page as usable
apglb4:	push ecx
	push edx
	xor edx, edx
	mov ecx, 8
apglp0:	dec ecx
	jz apglb1				; loop is done with no usable page marked
	cmp [uspg + 8 * ecx], edx
	jne apglp0				; mark a usable page as needed
	mov [uspg + 8 * ecx], edi
	jmp apglp0				; loop
apglb1:	pop edx
	pop ecx
	shr ecx, 12
apglp1:	push edi
	push ebx
	call crawlpg
	dec byte [eax]
	pop ebx
	pop edi
	add ebx, 0x1000
	jnc apglb2				; 64 bit add 0x1000
	inc edi
apglb2:	test edi, edi
	jz apglb4				; if the page is below 4 GiB, check for usability
	loop apglp1				; loop on size
	jmp apgfc0				; check for completion
apglb3:	dec ecx
	shl ecx, 12
	dec eax
	jmp apglb0				; did 52 bit decrement, go back to usability tests and such
apgfin:	pop edi
	pop ecx
	ret
apgfc0:	test eax, eax
	jz apgfin
	jmp apglb3
