	;; test/longmode.asm
	;; verify that long mode has successfully been entered

section .rodata
tststr:	db 'H', 0x0a, 'e', 0x02, 'l', 0x02, 'l', 0x02, 'o', 0x02, ' ', 0x00
	db 'l', 0x0a, 'o', 0x02, 'n', 0x02, 'g', 0x02, ' ', 0x00
	db 'm', 0x02, 'o', 0x02, 'd', 0x02, 'e', 0x02, '\n', 0x82
tstrlen:dq tstrlen - tststr
errstr:	db 'O', 0x1f, 'o', 0x1f, 'p', 0x1f, 's', 0x1f, '\n', 0x9c
estrlen:dd estrlen - errstr

section .text
global step1

step1:	mov sp, ax
	db 0x9c				; pushfd or pushfq
	db 0x66, 0x2d, 8, 0		; sub ax, 8
	cmp sp, ax
	jne err
	; the above code carefully checks for x86_64 mode without causing a fault
	; we have to copy out the success string in under 256 bytes (under 16 instructions is safe)
	mov rcx, [tstrlen]
	mov rsi, tststr
	mov edi, 0xb8000
	rep movsb
	cli
	hlt
	; now onto the error code, written for legacy/compatibility mode
[BITS 32]
err:	mov ecx, [estrlen]
	mov esi, errstr
	mov edi, 0xb8000
	rep movsb
	cli
	hlt
