ASM=nasm -f elf64
OBJFILES=step0.o
OBJALL=$(OBJFILES) test/longmode.o handoff.o
CC=x86_64-elf-gcc
CFLAGS=-mno-red-zone -ffreestanding -Os -pipe
LD=x86_64-elf-gcc -l gcc -z max-page-size=4096 -nostdlib -ffreestanding -O2

step0.o :	step0.asm
	nasm -f elf64 -o step0.o step0.asm

test/longmode.o :	test/longmode.asm
	$(ASM) -o test/longmode.o test/longmode.asm

test_longmode/boot/grub.cfg :
	cp test/grubdefault.cfg test_longmode/boot/grub.cfg

test_longmode/boot/elfexe :	test/longmode.o $(OBJFILES) test/longmode.ld
	$(LD) -o test_longmode/boot/elfexe test/longmode.o $(OBJFILES)

test_longmode.iso :	test_longmode/boot/elfexe test_longmode/boot/grub.cfg
	grub2-mkrescue -o test_longmode.iso test_longmode

test/longmode.ld :	test/linkdefault.ld
	cp test/linkdefault.ld test/longmode.ld

clean :
	rm -f $(OBJALL)
