	;; serial.asm
	;; an RS-232 serial logger

section .text
global log
global initSerial

initSerial:
	mov dx, 0x3f9
	mov al, 0
	out dx, al
	inc dx
	mov al, 1
	out dx, al
	inc dx
	mov al, 0x83
	out dx, al
	dec dx
	mov al, 0
	out dx, al
	dec dx
	mov al, 12
	out dx, al
	add dx, 3
	mov al, 0x03
	out dx, al
	add dl, 2
	mov al, 0x03
	out dx, al
	ret

log:	mov rdi, rax
	mov al, [rdi]
	mov dx, 0x403
lb1:	push al
lb0:	pause
	in al, dx
	cmp al, 0
	je lb0
	mov dx, 0x3f8
	pop al
	out dx, al
	mov al, [rdi]
	cmp al, 0
	jne lb1
	ret
